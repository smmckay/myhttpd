FROM httpd:2.4
LABEL maintainer="Sean McKay"
LABEL email="sean.mckay@citi.com"
LABEL version="0.1"

RUN apt-get update
RUN apt -y install mysql-server
COPY index.html /usr/local/apache2/htdocs
COPY start.sh /start.sh
RUN chmod +x /start.sh


ENTRYPOINT ["/start.sh"]
